<nav>
        <div class="nav-wrapper container">
          <a href="#" data-activates="mobile-demo" class="button-collapse"><i class="material-icons">menu</i></a>
          <ul class="left hide-on-med-and-down">
            <li><a href="/">En stream<i class="fa fa-twitch left"></i></a></li>
            <li><a href="/videos">Mes vidéos<i class="fa fa-video-camera left"></i></a></li>
            <li><a href="/boutique">La boutique<i class="fa fa-shopping-bag left"></i></a></li>
            <li><a href="/extension">Mon extension<i class="fa fa-puzzle-piece left"></i></a></li>
          </ul>

          <ul class="social-menu">
            <li class="social"><a href="https://www.snapchat.com/add/al3xmaw" target="_blank" class="social-icones snapchat"><i class="fa fa-snapchat-ghost"></i></a></li>
            <li class="social"><a href="https://www.instagram.com/alexmytb" target="_blank" class="social-icones instagram"><i class="fa fa-instagram"></i></a></li>
            <li class="social"><a href="https://www.twitch.tv/alexmytb_" target="_blank" class="social-icones twitch"><i class="fa fa-twitch"></i></a></li>
            <li class="social"><a href="https://www.youtube.com/c/AlexMAW" target="_blank" class="social-icones youtube"><i class="fa fa-youtube-play"></i></a></li>
            <li class="social"><a href="https://twitter.com/alexmytb" target="_blank" class="social-icones twitter"><i class="fa fa-twitter"></i></a></li>
          </ul>

          <ul class="side-nav" id="mobile-demo">
            <li><a href="/">En stream</a></li>
            <li><a href="/videos">Mes vidéos</a></li>
            <li><a href="/boutique">La boutique</a></li>
            <li><a href="/extension">Mon extension</a></li>
          </ul>
        </div>
      </nav>

      <script type="text/javascript">$(".button-collapse").sideNav();</script>
